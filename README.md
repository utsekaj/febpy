# FEBPY
Originally I thought about working towards a full Python interface to work with FEBio, but for my use case I find I really just needed the XPLT for post processing. I adapted some code from [interFEBio](https://github.com/andresutrera/interFEBio/tree/master/interFEBio) to access the data and have implemented a converter to VTK using [pyvista](https://docs.pyvista.org/version/stable/), which can enable working with postprocessing tools developed around VTK.

# Usage
Ensure `pyvista` and `numpy` are installed in your Python environment. 

The file example.py shows how one can load an xplt file and save the mesh region for a given solver step as a vtk object. Note this assumes you have run the file `test.feb` with FEBio and saved the resulting plotfile as `test.xplt` in the `test_data` folder.

Create an issue here if something is unclear or seems not to work.